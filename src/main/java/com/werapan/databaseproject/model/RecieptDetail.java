/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class RecieptDetail {

    private int Id;
    private int productId;
    private String productName;
    private float procuctPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;

    public RecieptDetail(int Id, int productId, String productName, float procuctPrice, int qty, float totalPrice, int recieptId) {
        this.Id = Id;
        this.productId = productId;
        this.productName = productName;
        this.procuctPrice = procuctPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }

    public RecieptDetail(int productId, String productName, float procuctPrice, int qty, float totalPrice, int recieptId) {
        this.Id = -1;
        this.productId = productId;
        this.productName = productName;
        this.procuctPrice = procuctPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }

    public RecieptDetail() {
        this.Id = -1;
        this.productId = 0;
        this.productName = "";
        this.procuctPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProcuctPrice() {
        return procuctPrice;
    }

    public void setProcuctPrice(float procuctPrice) {
        this.procuctPrice = procuctPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "Id=" + Id + ", productId=" + productId + ", productName=" + productName + ", procuctPrice=" + procuctPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + '}';
    }

    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProcuctPrice(rs.getFloat("procuct_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;

    }
}
